**Set up steps:**

    * npm install - to install dependencies
    * npm run serve - to serve app with webpack-serve 
    * npm run prod - to build app in production
 
**Libraries we are using**

    * React - front end library to make single page application.
    * Redux - state management using redux store
    * Redux-Saga - Redux middle ware Using saga to make test life cycle easier with help of generators
    * React-Router - React router for application routing.
    * Webpack - Build tool
    * Babel - To transpile ES6 & ES7 codes to ES5.
    * SCSS -  to make use of programing over css.