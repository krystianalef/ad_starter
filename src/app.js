import React from "react";
import {Route} from "react-router-dom";
import Home from "./components/Home/Home/Home";
import Info from "./components/Info/Info/Info";

const App = (props) => (
    <div>
        <Route location={props.location} key={props.location.key} component={({match}) =>
            <div>
                <Route exact path={`${match.path}`} component={Home}/>
                <Route exact path={`${match.path}/info`} component={Info}/>
            </div>
        }/>
    </div>
);

export default App;