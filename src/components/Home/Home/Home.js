import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {HomeGetAction} from "../../../store/actions/HomeAction";

import HomeItem from "../HomeItem/HomeItem";

export class Home extends Component {

    static propTypes = {
        loading: PropTypes.bool,
        error: PropTypes.string,
        list: PropTypes.array
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.HomeGetAction();
    }

    renderList(list) {
        return list.map(item => (
            <HomeItem {...item} key={item.id}/>
        ));
    }

    renderLoader = () => (
        <h1>Fetching</h1>
    );

    renderErrors = (error) => (
        <h1>{error}</h1>
    );

    render() {

        const {loading, error, list} = this.props;

        return (
            <div className="Home">
                <div className="Home__loading">
                    {
                        loading ? this.renderLoader() : null
                    }
                </div>

                <div className="Home__errors">
                    {
                        error ? this.renderErrors(error) : null
                    }
                </div>

                <div className="Home__list">
                    {
                        list.length ? this.renderList(list) : null
                    }
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        loading: state.HomeState.loading,
        error: state.HomeState.error,
        list: state.HomeState.list
    };
}

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({HomeGetAction}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);