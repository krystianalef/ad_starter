import React, {Component} from "react";
import PropTypes from "prop-types";

export default class HomeItem extends Component {

    static propTypes = {
        id: PropTypes.number,
        userId: PropTypes.number,
        title: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {
            counter: 0
        };
    }

    increment = () => {
        this.setState({
            counter: ++this.state.counter
        });
    };

    render() {

        return (
            <div onClick={this.increment} className="HomeItem">
                <img className="HomeItem__img" src="img/HomeItem__icon.png" alt="icon"/>

                <div className="HomeItem__box">
                    <div className="HomeItem__counter">
                        {this.state.counter}
                    </div>

                    <div className="HomeItem__title">
                        {this.props.title}
                    </div>
                </div>
            </div>
        );
    }
}