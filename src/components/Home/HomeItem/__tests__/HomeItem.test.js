import React from "react";
import renderer from "react-test-renderer";
import HomeItem from "../HomeItem";

describe("HomeItem", () => {
    it("renders correctly", () => {
        const component = renderer.create(
            <HomeItem title="Some title"/>
        ).toJSON();

        expect(component).toMatchSnapshot();
    });

    it("renders correctly when empty title", () => {
        const component = renderer.create(
            <HomeItem/>
        ).toJSON();

        expect(component).toMatchSnapshot();
    });

    it("renders correctly after increment", () => {
        const component = renderer.create(
            <HomeItem/>
        );

        component.getInstance().increment();
        component.getInstance().increment();

        expect(component.toJSON()).toMatchSnapshot();
    });
});