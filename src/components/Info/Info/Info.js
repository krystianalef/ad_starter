import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {InfoGetAction} from "../../../store/actions/InfoAction";

import InfoItem from "../InfoItem/InfoItem";

export class Info extends Component {

    static propTypes = {
        loading: PropTypes.bool,
        error: PropTypes.string,
        list: PropTypes.array
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.InfoGetAction();
    }

    renderList(list) {
        return list.map(item => (
            <InfoItem {...item} key={item.id}/>
        ));
    }

    renderLoader = () => (
        <h1>Fetching</h1>
    );

    renderErrors = (error) => (
        <h1>{error}</h1>
    );

    render() {

        const {loading, error, list} = this.props;

        return (
            <div className="Info">
                <div className="Info__loading">
                    {
                        loading ? this.renderLoader() : null
                    }
                </div>

                <div className="Info__errors">
                    {
                        error ? this.renderErrors(error) : null
                    }
                </div>

                <div className="Info__list">
                    {
                        list.length ? this.renderList(list) : null
                    }
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        loading: state.InfoState.loading,
        error: state.InfoState.error,
        list: state.InfoState.list
    };
}

export function mapDispatchToProps(dispatch) {
    return bindActionCreators({InfoGetAction}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Info);