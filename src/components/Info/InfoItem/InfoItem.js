import React from "react";
import PropTypes from "prop-types";

const InfoItem = ({title}) => (
    <div className="InfoItem">
        <div className="InfoItem__title">{title}</div>
    </div>
);

InfoItem.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string
};

export default InfoItem;