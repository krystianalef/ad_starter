import React from "react";
import renderer from "react-test-renderer";
import InfoItem from "../InfoItem";

describe("InfoItem", () => {
    it("renders correctly", () => {
        const component = renderer.create(
            <InfoItem title="Some title"/>
        ).toJSON();

        expect(component).toMatchSnapshot();
    });

    it("renders correctly when empty title", () => {
        const component = renderer.create(
            <InfoItem/>
        ).toJSON();

        expect(component).toMatchSnapshot();
    });
});