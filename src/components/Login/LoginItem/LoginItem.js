import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const LoginItem = ({title}) => (
    <div className="LoginItem">
        <div className="LoginItem__title">{title}</div>

        <Link className="LoginItem__link" to="/app">App</Link>

        <Link className="LoginItem__link" to="/app/info">Info</Link>
    </div>
);

LoginItem.propTypes = {
    title: PropTypes.string
};

export default LoginItem