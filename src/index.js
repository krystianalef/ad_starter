import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {Provider} from "react-redux";

import store from "./store/store";
import "./styles/index.scss";

import AuthRoute from "./utils/auth/AuthRoute";
import App from "./app";
import Login from "./components/Login/Login/Login";

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Switch>
                <AuthRoute path="/app" component={App}/>
                <Route path="/login" component={Login}/>
                <Route component={Login}/>
            </Switch>
        </Provider>
    </BrowserRouter>, document.getElementById("root")
);