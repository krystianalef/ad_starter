export const HOME_GET = "HOME_GET";
export const HOME_GET_LOADING = "HOME_GET_LOADING";
export const HOME_GET_SUCCESS = "HOME_GET_SUCCESS";
export const HOME_GET_ERROR = "HOME_GET_ERROR";

export const HomeGetAction = () => {
    return {
        type: HOME_GET
    };
};

export const HomeGetLoadingAction = () => {
    return {
        type: HOME_GET_LOADING
    };
};

export const HomeGetSuccessAction = (list) => {
    return {
        type: HOME_GET_SUCCESS,
        list: list
    };
};

export const HomeGetErrorAction = (error) => {
    return {
        type: HOME_GET_ERROR,
        error: error
    };
};