export const INFO_GET = "INFO_GET";
export const INFO_GET_LOADING = "INFO_GET_LOADING";
export const INFO_GET_SUCCESS = "INFO_GET_SUCCESS";
export const INFO_GET_ERROR = "INFO_GET_ERROR";

export const InfoGetAction = () => {
    return {
        type: INFO_GET
    };
};

export const InfoGetLoadingAction = () => {
    return {
        type: INFO_GET_LOADING
    };
};

export const InfoGetSuccessAction = (list) => {
    return {
        type: INFO_GET_SUCCESS,
        list: list
    };
};

export const InfoGetErrorAction = (error) => {
    return {
        type: INFO_GET_ERROR,
        error: error
    };
};