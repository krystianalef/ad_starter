import * as home from "../HomeAction";

describe("HomeAction", () => {
    it("should return type get", () => {
        const expectAction = {
            type: home.HOME_GET
        };
        expect(home.HomeGetAction()).toEqual(expectAction);
    });

    it("should return type loading", () => {
        const expectAction = {
            type: home.HOME_GET_LOADING
        };
        expect(home.HomeGetLoadingAction()).toEqual(expectAction);
    });

    it("should return type succeed", () => {
        const list = [];
        const expectAction = {
            type: home.HOME_GET_SUCCESS,
            list: list
        };
        expect(home.HomeGetSuccessAction(list)).toEqual(expectAction);
    });

    it("should return type failed", () => {
        const message = "Some error";
        const expectAction = {
            type: home.HOME_GET_ERROR,
            error: message
        };
        expect(home.HomeGetErrorAction(message)).toEqual(expectAction);
    });
});