import * as home from "../InfoAction";

describe("InfoAction", () => {
    it("should return type get", () => {
        const expectAction = {
            type: home.INFO_GET
        };
        expect(home.InfoGetAction()).toEqual(expectAction);
    });

    it("should return type loading", () => {
        const expectAction = {
            type: home.INFO_GET_LOADING
        };
        expect(home.InfoGetLoadingAction()).toEqual(expectAction);
    });

    it("should return type succeed", () => {
        const list = [];
        const expectAction = {
            type: home.INFO_GET_SUCCESS,
            list: list
        };
        expect(home.InfoGetSuccessAction(list)).toEqual(expectAction);
    });

    it("should return type failed", () => {
        const message = "Some error";
        const expectAction = {
            type: home.INFO_GET_ERROR,
            error: message
        };
        expect(home.InfoGetErrorAction(message)).toEqual(expectAction);
    });
});