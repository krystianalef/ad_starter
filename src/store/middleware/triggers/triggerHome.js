import {put, call} from "redux-saga/effects";
import {HomeGetLoadingAction, HomeGetSuccessAction, HomeGetErrorAction} from "../../actions/HomeAction";
import api from "../../api";

export function* getHome() {
    yield put(HomeGetLoadingAction());

    try {
        const home = yield call(api.get, "/posts");
        yield put(HomeGetSuccessAction(home.data));
    } catch (e) {
        yield put(HomeGetErrorAction(e.message));
    }
}