import {put, call} from "redux-saga/effects";
import {InfoGetLoadingAction, InfoGetSuccessAction, InfoGetErrorAction} from "../../actions/InfoAction";
import axios from "axios";

export function* getInfo() {
    yield put(InfoGetLoadingAction());

    try {
        const info = yield call(axios.get, "/src/utils/data/info.json");
        yield put(InfoGetSuccessAction(info.data));
    } catch (e) {
        yield put(InfoGetErrorAction(e.message));
    }
}