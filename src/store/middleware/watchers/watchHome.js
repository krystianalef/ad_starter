import {takeEvery} from "redux-saga/effects";
import {HOME_GET} from "../../actions/HomeAction";
import {getHome} from "../triggers/triggerHome";

export function* watchHome() {
    yield takeEvery(HOME_GET, getHome);
}