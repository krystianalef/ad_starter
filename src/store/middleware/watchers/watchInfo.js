import {takeEvery} from "redux-saga/effects";
import {INFO_GET} from "../../actions/InfoAction";
import {getInfo} from "../triggers/triggerInfo";

export function* watchInfo() {
    yield takeEvery(INFO_GET, getInfo);
}