import {INFO_GET_LOADING, INFO_GET_SUCCESS, INFO_GET_ERROR} from "../actions/InfoAction";

let init = {
    loading: false,
    list: [],
    error: ""
};

export function InfoReducer(state = init, action) {

    switch (action.type) {

        case INFO_GET_LOADING: {
            return {
                ...state,
                loading: true,
                error: ""
            };
        }

        case INFO_GET_SUCCESS: {
            return {
                ...state,
                list: action.list,
                loading: false
            };
        }

        case INFO_GET_ERROR: {
            return {
                ...state,
                loading: false,
                error: action.error
            };
        }
    }

    return state;
}