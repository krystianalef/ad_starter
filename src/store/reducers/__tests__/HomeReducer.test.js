import {HomeReducer} from "../HomeReducer";
import {HomeGetLoadingAction, HomeGetErrorAction} from "../../actions/HomeAction";

describe("HomeReducer", () => {
    it("should return the initial state", () => {
        const expectState = {
            loading: false,
            list: [],
            error: ""
        };
        expect(HomeReducer(undefined, {})).toEqual(expectState);
    });

    it("should handle loading", () => {
        const expectState = {
            loading: true,
            list: [],
            error: ""
        };
        expect(HomeReducer(undefined, HomeGetLoadingAction())).toEqual(expectState);
    });

    it("should handle error", () => {
        const expectState = {
            loading: false,
            list: [],
            error: "Error message"
        };
        expect(HomeReducer(undefined, HomeGetErrorAction("Error message"))).toEqual(expectState);
    });
});