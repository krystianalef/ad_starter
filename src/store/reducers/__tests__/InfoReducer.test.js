import {InfoReducer} from "../InfoReducer";
import {InfoGetLoadingAction, InfoGetErrorAction} from "../../actions/InfoAction";

describe("InfoReducer", () => {
    it("should return the initial state", () => {
        const expectState = {
            loading: false,
            list: [],
            error: ""
        };
        expect(InfoReducer(undefined, {})).toEqual(expectState);
    });

    it("should handle loading", () => {
        const expectState = {
            loading: true,
            list: [],
            error: ""
        };
        expect(InfoReducer(undefined, InfoGetLoadingAction())).toEqual(expectState);
    });

    it("should handle error", () => {
        const expectState = {
            loading: false,
            list: [],
            error: "Error message"
        };
        expect(InfoReducer(undefined, InfoGetErrorAction("Error message"))).toEqual(expectState);
    });
});