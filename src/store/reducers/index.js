import {combineReducers} from "redux";
import {HomeReducer} from "./HomeReducer";
import {InfoReducer} from "./InfoReducer";

export default combineReducers({
    HomeState: HomeReducer,
    InfoState: InfoReducer
});