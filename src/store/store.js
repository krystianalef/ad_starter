import {createStore, applyMiddleware} from "redux";
import {logger} from "redux-logger";
import createSagaMiddleware from "redux-saga";
import reducer from "./reducers/index";
import saga from "./middleware/index";

let sagaMiddleware = createSagaMiddleware();
let store;

if (process.env.NODE_ENV == "production") {
    store = createStore(reducer, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(saga);
} else {
    store = createStore(reducer, applyMiddleware(logger, sagaMiddleware));
    sagaMiddleware.run(saga);
}

export default store;