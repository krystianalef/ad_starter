import React, {Component} from "react";
import {Route, Redirect} from "react-router-dom";
import checkAuth from "./checkAuth";

const AuthRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        checkAuth()
            ? <Component {...props}/>
            : <Redirect to={{pathname: "/login", state: {from: props.location}}}/>
    )}/>
);

export default AuthRoute