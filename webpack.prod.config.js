const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const autoprefixer = require("autoprefixer");
require("babel-polyfill");

module.exports = {
    entry: ["babel-polyfill", __dirname + "/src/index.js"],
    output: {
        path: __dirname + "/dist/",
        filename: "js/app.js",
        publicPath: "/"
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: true,
                            plugins: () => [autoprefixer("last 2 versions")]
                        }
                    }, {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-bulk-import-loader"
                    }]
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    },
                    {
                        loader: "eslint-loader"
                    }
                ]
            },
            {
                test: /\.(png|jpg|ttf|eot|woff|svg)$/,
                exclude: /node_modules/,
                loader: "file-loader"
            }
        ]
    },
    devtool: "source-map",
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/src/index.html"
        }),
        new ExtractTextPlugin("css/app.css"),
        new CopyWebpackPlugin([{
            from: "src/components/**/src/*",
            flatten: true,
            to: "img"
        }]),
        new UglifyJSPlugin({
            extractComments: true
        }),
        //WHEN NEED ANALYZE
        // new BundleAnalyzerPlugin()
    ]
};