const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const autoprefixer = require("autoprefixer");
require("babel-polyfill");

module.exports = {
    entry: ["babel-polyfill", __dirname + "/src/index.js"],
    output: {
        path: __dirname + "/dist/",
        filename: "js/app.js",
        publicPath: "/"
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader",
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: "postcss-loader",
                    options: {
                        sourceMap: true,
                        plugins: () => [autoprefixer("last 2 versions")]
                    }
                }, {
                    loader: "sass-loader",
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: "sass-bulk-import-loader"
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    },
                    {
                        loader: "eslint-loader"
                    }
                ]
            },
            {
                test: /\.(png|jpg|ttf|eot|woff|svg)$/,
                exclude: /node_modules/,
                loader: "file-loader"
            }
        ]
    },
    devtool: "cheap-module-eval-source-map",
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: __dirname + "/src/index.html"
        }),
        new CopyWebpackPlugin([{
            from: "src/components/**/src/*",
            flatten: true,
            to: "img"
        }])
    ],
    devServer: {
        historyApiFallback: true,
        inline: true,
        hot: true
    }
};